
A collection of python scripts to generate [word lists by frequency][] based on [ngram datasets][] published by
Google. The dataset is available under the Creative Commons Attribution 3.0 Unported License ([CC-BY][]).

# Usage

To extract the most 200 common words from the French dataset from 1800 to 2000:

    $ pip install [--user] -r requirements.txt
    $ ./google_word_frequency.py --lang fre --startyear 1800 --endyear 2000 -n 200

Output will be written to the file `wordlist-fre-n200-y1800-2000.json`. The
dataset is downloaded and processed as a transformation of streams which can
take a while, depending on your network connectivity and the chosen language.

# Post-process / convert data

The generated lists can then be further processed to remove duplicates and convert them
to different formats.

## CSV

You'll need [jq](https://stedolan.github.io/jq/) for this:

    $ ./post_process.py list.json | jq -r '.[] | @csv' > output.csv

## [Wiktionary][]

    $ ./post_process.py list.json | ./format_wiktionary.py -n 1000 --lang fr

[Sample output][]

# Data quality considerations

From the [Culturomics FAQ][]:

## OCR issues before 1800

> By and large the OCR, the dating accuracy, and the volume of data are all much bigger issues before
> 1800 than after. That's why our paper doesn't use any data before 1800.

## Corpus after 2000

> Before 2000, most of the books in Google Books come from library holdings. But when the Google
> Books project started back in 2004, Google started receiving lots of books from publishers. This
> dramatically affects the composition of the corpus in recent years and is why our paper doesn't
> use any data from after 2000.

## No bibliography

> But, as folks like Ted Underwood and Matthew Jockers have noted, we haven't released a full, 5.2
> million book bibliography telling the public which books are in which corpora. And we agree with
> them: that's a big issue. We hoped to release a full list of all books in the various corpora
> together with the paper, but have not received permission to do so yet.

[Culturomics FAQ]: http://www.culturomics.org/Resources/faq
[ngram datasets]: http://storage.googleapis.com/books/ngrams/books/datasetsv2.html
[word lists by frequency]: https://en.wikipedia.org/wiki/Word_lists_by_frequency
[Sample output]: https://en.wiktionary.org/wiki/User:Jberkel/Frequency_lists
[Wiktionary]: https://en.wiktionary.org/
[CC-BY]: http://creativecommons.org/licenses/by/3.0/
