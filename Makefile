TESTS = google_word_frequency_test post_process_test

test: pep8
	@python -m unittest $(TESTS)
	@python3 -m unittest $(TESTS)

pep8:
	@pep8 *.py
