#!/usr/bin/env python
""" Parses word lists, and performs some prepocessing and cleanup
    The processed list is written to stdout.
"""

import json
import re
from collections import OrderedDict

POS_PATTERN = re.compile("_[A-Z]+$")


def sort_list(word_list):
    return sorted(word_list, key=lambda word_count: -word_count[1])


def deduplicate(word_list):
    result = OrderedDict()
    for (word, count) in word_list:
        if word not in result:
            result[word] = count
    return list(result.items())


def strip_pos_tags(word_list):
    def strip_pos_tag(word):
        matcher = POS_PATTERN.search(word)
        if matcher:
            return word[:matcher.start()]
        else:
            return word

    return [(strip_pos_tag(word), count) for (word, count) in word_list]


def normalize(word_list):
    return deduplicate(sort_list(strip_pos_tags(word_list)))


if __name__ == '__main__':
    import sys
    import codecs

    with sys.stdin as f:
        writer = codecs.getwriter('UTF8')(sys.stdout)
        writer.write(json.dumps(normalize(json.load(f)), indent=True))
