#!/usr/bin/env python
import unittest
import collections
from google_word_frequency import top_n_words, all_records, \
    record_filter, parse_config
from google_ngram_downloader.util import Record


class GoogleWordFrequencyTest(unittest.TestCase):

    def test_top_n_words_empty(self):
        self.assertEqual([], top_n_words(n=2, records=[]))

    def test_top_n_words(self):
        result = top_n_words(n=2, records=[
            Record('foo', 1970, 4, 5),
            Record('foo', 1971, 6, 5),
            Record('bar', 1970, 10, 5),
            Record('bar', 1971, 30, 5),
            Record('bar', 1970, 20, 5),
            Record('baz', 2000, 20, 5)
        ], threshold=0)

        self.assertEqual([('bar', 60), ('baz', 20)], result)

    def test_all_records(self):
        ger = all_records('ger', verbose=False)
        self.assertEqual(Record(ngram=u'A.J.B._NOUN',
                                year=1899, match_count=1,
                                volume_count=1), next(ger))

        self.assertEqual(Record(ngram=u'A.J.B._NOUN',
                                year=1909, match_count=1,
                                volume_count=1), next(ger))
        ger.close()

    def test_record_filter(self):
        Config = collections.namedtuple('Config', 'debug startyear endyear')
        cfg = Config(debug=False, startyear=1950, endyear=2000)
        self.assertFalse(record_filter(Record('foo', 1949, 4, 5), cfg))
        self.assertTrue(record_filter(Record('foo', 1950, 4, 5), cfg))
        self.assertTrue(record_filter(Record('foo', 2000, 4, 5), cfg))
        self.assertFalse(record_filter(Record('foo', 2001, 4, 5), cfg))
        self.assertTrue(record_filter(Record('foo', 1970, 4, 5), cfg))

    def test_parse_config_sets_defaults(self):
        cfg = parse_config(['--lang', 'ger'])
        self.assertEqual('ger', cfg.lang)
        self.assertEqual(0, cfg.startyear)
        self.assertEqual(2012, cfg.endyear)

    def test_parse_config_startyear(self):
        cfg = parse_config(['--lang', 'ger', '--startyear', '1900'])
        self.assertEqual(1900, cfg.startyear)

    def test_parse_config_startyear(self):
        cfg = parse_config(['--lang', 'ger', '--endyear', '1900'])
        self.assertEqual(1900, cfg.endyear)


if __name__ == '__main__':
    unittest.main()
