#!/usr/bin/env python
import unittest
from post_process import normalize, deduplicate, sort_list


class PostProcessTest(unittest.TestCase):

    def test_sort_list(self):
        self.assertEqual([('FOO', 256), ('FOO', 123), ('BAZ', 1)],
                         sort_list([('FOO', 123), ('FOO', 256), ('BAZ', 1)]))

    def test_deduplicate(self):
        self.assertEqual([('BAZ', 512), ('FOO', 123)],
                         deduplicate([('BAZ', 512), ('FOO', 123),
                                      ('FOO', 256)]))

    def test_normalize_empty(self):
        self.assertEqual([], normalize([]))

    def test_normalize_simple(self):
        self.assertEqual([('bar', 2), ('foo', 1)],
                         normalize([('foo', 1), ('bar', 2)]))

    def test_normalize_filters_POS(self):
        self.assertEqual([('foo', 2)],
                         normalize([('foo_NOUN', 1), ('foo', 2)]))


if __name__ == '__main__':
    unittest.main()
