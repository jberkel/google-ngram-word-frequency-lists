#!/usr/bin/env python
""" Creates top-N frequency lists based on Google's Ngram datasets published on
    http://storage.googleapis.com/books/ngrams/books/datasetsv2.html
    Only 1-grams are taken into account.
"""

from google_ngram_downloader import readline_google_store
from itertools import groupby
from string import ascii_lowercase
import json
import sys

VALID_LANGUAGES = ['fre', 'ger', 'eng', 'eng-us', 'eng-gb', 'eng-fiction',
                   'chi-sim', 'heb', 'ita', 'rus', 'spa']

DEFAULT_N = 3000
MIN_YEAR = 0
MAX_YEAR = 2012
FILTER_THRESHOLD = 3000


def all_records(lang, indices=[x for x in ascii_lowercase], verbose=True):
    """Returns a generator producing all records for language & indices"""
    if lang not in VALID_LANGUAGES:
        raise Exception('Invalid language %s' % lang)
    for _, _, records in readline_google_store(ngram_len=1, lang=lang,
                                               indices=indices,
                                               verbose=verbose):
        for record in records:
            yield record


def count_ngrams(grouped_records):
    return ((ngram, sum(r.match_count for r in records))
            for ngram, records in grouped_records)


def filter_high_frequency(ngram_counts, threshold):
    return ((word, count) for (word, count) in ngram_counts
            if count > threshold)


def record_filter(record, config):
    ret = record.year in range(config.startyear, config.endyear+1)
    if config.debug and ret:
        print("matched: %s" % str(record))
    return ret


def ngram_input(config):
    records = all_records(config.lang, verbose=config.debug)
    return (r for r in records if record_filter(r, config))


def top_n_words(n, records, threshold=FILTER_THRESHOLD):
    """returns the n most frequent words for a given language"""
    return sorted(list(filter_high_frequency(
        count_ngrams(groupby(records, key=lambda r: r.ngram)), threshold)),
                  key=lambda word_count: -word_count[1])[:n]


def write_json_file(words, config):
    timespec = "-y%d-%d" % (config.startyear, config.endyear)
    filename = "wordlist-%s-n%d%s.json" % (config.lang, config.n, timespec)
    with open(filename, 'w') as f:
        json.dump(words, f, indent=True)


def parse_config(args):
    from argparse import ArgumentParser
    parser = ArgumentParser(
        description='Extract top frequency words from Google Ngram data')
    parser.add_argument(
        '-n', dest='n', type=int, default=DEFAULT_N,
        help='number of words to extract (default %d)' % DEFAULT_N)
    parser.add_argument(
        '--startyear', dest='startyear', type=int, default=MIN_YEAR,
        help='only include words after specified year (default %d)' % MIN_YEAR)
    parser.add_argument(
        '--endyear', dest='endyear', type=int, default=MAX_YEAR,
        help='only include words up to specified year (default %d)' % MAX_YEAR)
    parser.add_argument(
        '--lang', dest='lang', required=True, choices=VALID_LANGUAGES,
        help='language code')
    parser.add_argument('--debug', action="store_true")
    config = parser.parse_args(args)

    if config.startyear < 0:
        raise Exception('invalid startyear, must be > 0')
    if config.endyear > MAX_YEAR:
        raise Exception('invalid endyear, must be < %d' % MAX_YEAR)
    if config.endyear < config.startyear:
        raise Exception('endyear < startyear')
    return config

if __name__ == '__main__':
    config = parse_config(sys.argv[1:])
    top_n = top_n_words(config.n, ngram_input(config))
    write_json_file(top_n, config)
