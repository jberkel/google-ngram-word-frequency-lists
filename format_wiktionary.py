#!/usr/bin/env python
""" Reads a word list from stdin and returns the output in a format suitable
    for wiktionary
"""

DEFAULT_N = 1000


def mediawiki_table(lang_code, list):
    table_header = """{| class="wikitable"
|-
! rank
! word
"""
    table_footer = "|}"
    body = ""
    for (rank, (word, count)) in enumerate(list):
        body += """|-\n|%d.\n|{{l|%s|%s}}\n""" % (rank+1, lang_code, word)
    return table_header + body + table_footer


if __name__ == '__main__':
    import sys
    import codecs
    import json
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Format word list for wiktionary')
    parser.add_argument('--lang', required=True, help='language code')
    parser.add_argument('-n', dest='n', type=int, default=DEFAULT_N,
                        help='number of words to extract (default %d)'
                             % DEFAULT_N)
    parser.add_argument('file', nargs='?',
                        help='the file to process, empty for stdin')
    args = parser.parse_args()

    if args.file:
        source = open(args.file, 'r')
    else:
        source = sys.stdin

    with source as f:
        word_list = json.load(f)
        writer = codecs.getwriter('UTF8')(sys.stdout)
        writer.write(mediawiki_table(lang_code=args.lang,
                                     list=word_list[:args.n]))
